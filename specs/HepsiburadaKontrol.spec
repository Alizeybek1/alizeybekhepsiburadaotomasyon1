Hepsiburada Testi
=====================
Created by testinium on 11.01.2021

This is an executable specification file which follows markdown syntax.
Every heading in this file denotes a scenario. Every bulleted point denotes a step.

Hesap Girişi Kontrolü
----------------------
* Hepsiburada sayfasina giris yap
* Hepsiburada sayfasinin acildigini kontrol et
* Giris yap sayfasini ac
* Giris yap sayfasinin acildigini kontrol et
* "deneme438@hotmail.com" ve "121B3211" ile giris yap
* Hesabim sayfasina git
* Hesabim sayfasinin acildigini kontrol et

