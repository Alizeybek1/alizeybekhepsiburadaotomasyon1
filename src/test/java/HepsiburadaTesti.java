import com.thoughtworks.gauge.BeforeScenario;
import com.thoughtworks.gauge.Step;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class HepsiburadaTesti extends BaseTest {

    WebDriver driver;

    @BeforeScenario
    public void hazirlik() {
        System.setProperty("webdriver.chrome.driver", "libs/chromedriver.exe");
        this.driver = new ChromeDriver();
    }

    @Step("Hepsiburada sayfasina giris yap")
    public void baslangic() {
        driver.get("https://www.hepsiburada.com/");
    }

    @Step("Hepsiburada sayfasinin acildigini kontrol et")
    public void anaSayfaAcilisiKontrolu()  {

        WebElement alert = driver.findElement(By.cssSelector("span.sf-OldMyAccount-PhY-T"));
        Assert.assertTrue("Ana sayfa açıldı.", true);
        System.out.println(alert.getText());

    }

    @Step("Giris yap sayfasini ac")
    public void girisYap() throws InterruptedException {

        driver.findElement(By.cssSelector("span.sf-OldMyAccount-PhY-T")).click();
        Thread.sleep(1000);
        driver.findElement(By.cssSelector("a#login")).click();

    }

    @Step("Giris yap sayfasinin acildigini kontrol et")
    public void girisYapSayfaKontrolu() throws InterruptedException {

        Thread.sleep(2000);
        WebElement alert = driver.findElement(By.cssSelector("span._17OD2T5uhxYRT6atZy_yA7.hpa5gnmuOMPuH0E31USHp"));
        Assert.assertTrue("Giriş yap sayfası açıldı.", true);
        System.out.println(alert.getText());

    }

    @Step("<kullaniciAdi> ve <sifre> ile giris yap")
    public void kullaniciGirisi(String kullaniciAdi, String sifre) {

        driver.findElement(By.id("txtUserName")).sendKeys(kullaniciAdi);
        driver.findElement(By.id("txtPassword")).sendKeys(sifre);
        driver.findElement(By.id("btnLogin")).click();

    }

    @Step("Hesabim sayfasina git")
    public void hesabimSayfasinaGiris() throws InterruptedException {

        Thread.sleep(2000);
        driver.findElement(By.cssSelector("div#myAccount")).click();
        Thread.sleep(1000);
        driver.findElement(By.linkText("Hesabım")).click();

    }

    @Step("Hesabim sayfasinin acildigini kontrol et")
    public void hesabimSayfasiKontrolu() throws InterruptedException {

        Thread.sleep(1000);
        WebElement alert = driver.findElement(By.cssSelector("span.hb-font-h1"));
        Assert.assertTrue("Hesabım sayfası açıldı.", true);
        System.out.println(alert.getText());

    }
}
